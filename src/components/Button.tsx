import React from 'react';
import './Button.scss';

export interface ButtonProps {
	className?: string;
	content: string;
	variant?: 'blue' | 'pink';
}

export function Button(props: ButtonProps) {
	return <a href='' className={'button ' + (props.variant || 'blue')}>{props.content}</a>;
}
