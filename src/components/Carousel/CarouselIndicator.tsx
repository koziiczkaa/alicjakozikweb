import React from 'react';
import './CarouselIndicator.scss';

export interface CarouselIndicatorProps {
	className?: string;
	onClick: () => void;
	id: string;
}

export function CarouselIndicator(props: CarouselIndicatorProps) {
	return <a className={'carousel__indicator ' + props.className} onClick={props.onClick}/>;
}
