import React, {CSSProperties} from 'react';
import './CarouselSlide.scss';

export interface CarouselSlideProps {
	img: string;
	style: CSSProperties;
}

export function CarouselSlide(props: CarouselSlideProps) {
	return <li className='carousel__slide' style={props.style}>
		<img className='carousel__image' src={props.img} alt=''/>
	</li>;
}
