import React from "react";
import {SectionTitle} from "./SectionTitle";
import './AboutSection.scss';
import '../Variables.scss';

export function AboutSection() {
    return <section className='about'>
        <SectionTitle subtitle={"Alicja"} title={"About me"} color={"var(--clr-light)"}/>
        <div className='about-section'>
            <img src={} alt={}/>
        </div>
    </section>;
}