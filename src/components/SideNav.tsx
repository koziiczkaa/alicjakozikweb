import React from 'react';
import './SideNav.scss';

export interface SideNavProps {
	opened: boolean;
}

export function SideNav(props: SideNavProps) {
	return <div className={'sidenav' + (props.opened ? ' sidenav--active' : '')}>
		<ul className='sidenav__list'>
			<li className='sidenav__list-item'><a href='#' className='sidenav__list-link'>Home</a></li>
			<li className='sidenav__list-item'><a href='#' className='sidenav__list-link'>Portfolio</a></li>
			<li className='sidenav__list-item'><a href='#' className='sidenav__list-link'>Services</a></li>
			<li className='sidenav__list-item'><a href='#' className='sidenav__list-link'>Contact</a></li>
		</ul>
	</div>;
}
