import React from 'react';
import './NavBar.scss';
import {HamburgerMenu} from './HamburgerMenu';
import {SideNav} from './SideNav';
import {disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';

export function NavBar() {
	const [open, setOpen] = React.useState<boolean>(false);

	function handleClick() {
		setOpen(!open);
		if (open) {
			enableBodyScroll(document.body);
		} else {
			disableBodyScroll(document.body);
		}
	}

	return <>
		<header className='header'>
			<object type='image/svg+xml' data='../../resources/images/logo.svg' className='header__logo'/>
			<HamburgerMenu opened={open} onClick={handleClick}/>
		</header>
		<SideNav opened={open}/>
	</>;
}
