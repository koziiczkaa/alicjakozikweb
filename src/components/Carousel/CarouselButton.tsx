import React from 'react';
import './CarouselButton.scss';

export interface CarouselButtonProps {
	className?: string;
	img: string;
	onClick: () => void;
}

export function CarouselButton(props: CarouselButtonProps) {
	return <button className={'carousel__button ' + props.className} onClick={props.onClick}>
		<img src={props.img} alt=''/>
	</button>;
}
