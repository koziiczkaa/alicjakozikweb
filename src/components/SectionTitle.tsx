import React from "react";
import './SectionTitle.scss';

export interface SectionTitleProps{
    subtitle: string;
    title: string;
    color: string;
}

export function SectionTitle(props: SectionTitleProps){
    return <div className='section__title'>
        <h4 className='section__title-h4'>{props.subtitle}</h4>
        <h1 className='section__title-h1' style={{color: props.color}}>{props.title}</h1>
    </div>;
}