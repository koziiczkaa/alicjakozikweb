import React from 'react';
import './HomeSection.scss';
import {Button} from './Button';

export function HomeSection() {
	return <section className='home'>
		<div className='home__bkg'>
			<p>Hello!</p>
			<p>I'm Alicja</p>
		</div>
		<div className='home__title'>
			<h1 className='home__heading'>Hello! I'm <span className='home__heading-span'> Alicja Kozik</span>.</h1>
			<p className='home__aboutme'>I'm a Junior Front-End Developer based in Racibórz, Poland.</p>
		</div>

		<div className='home__buttons'>
			<Button className='button' content='View more'/>
		</div>
	</section>;
}
