import React from 'react';
import './PortfolioSection.scss';
import {Button} from './Button';
import {Carousel} from './Carousel/Carousel';
import andret from '../../resources/images/andret.png';
import pic1 from '../../resources/images/1.png';
import pic2 from '../../resources/images/2.png';
import {SectionTitle} from "./SectionTitle";

export function PortfolioSection() {
	return <div className='portfolio'>
		<SectionTitle subtitle={"Portfolio"} title={"Latest projects"}/>
		<div className='portfolio__button'>
			<Button className='button' content='View all projects'/>
		</div>
		<Carousel elements={[
			andret, pic1, pic2
		]}/>
	</div>;
}
