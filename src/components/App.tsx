import * as React from 'react';
import {NavBar} from './NavBar';
import {HomeSection} from './HomeSection';
import {PortfolioSection} from './PortfolioSection';
import {AboutSection} from "./AboutSection";

class App extends React.PureComponent {
	render() {
		return (
			<>
				<NavBar/>
				<HomeSection/>
				<PortfolioSection/>
				<AboutSection/>
			</>
		);
	}
}

export default App;
