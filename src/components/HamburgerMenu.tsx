import React from 'react';
import './HamburgerMenu.scss';

export interface HamburgerMenuProps {
	onClick: () => void;
	opened: boolean;
}

export function HamburgerMenu(props: HamburgerMenuProps): JSX.Element {
	return <div className='header__small'>
		<button className={'hamburger' + (props.opened ? ' hamburger--active' : '')} onClick={props.onClick}>
			<span className='hamburger__box'>
				<span className='hamburger__inner'/>
			</span>
		</button>
	</div>;
}
