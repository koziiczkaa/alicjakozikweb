import * as React from 'react';
import {SizeMe} from 'react-sizeme';
import {CarouselSlide} from './CarouselSlide';
import {CarouselButton} from './CarouselButton';
import {CarouselIndicator} from './CarouselIndicator';

import './Carousel.scss';

import left from '../../../resources/images/left.svg';
import right from '../../../resources/images/right.svg';

export interface CarouselProps {
	elements: string[];
}

export function Carousel(props: CarouselProps) {
	const [slide, setSlide] = React.useState<number>(0);
	const [prevSlide, setPrevSlide] = React.useState<number>(0);
	const [slideWidth, setSlideWidth] = React.useState<number>(0);
	React.useEffect(() => {
		console.log(slide);
		console.log(prevSlide);
		console.log(slideWidth * slide);
	});

	function handleClickPrev() {
		setSlide((slide + props.elements.length - 1) % props.elements.length);
		setPrevSlide(slide);
	}

	function handleClickNext() {
		setSlide((slide + 1) % props.elements.length);
		setPrevSlide(slide);
	}

	function handleIndicatorClick(slideId: number) {
		setSlide(slideId);
		setPrevSlide(slide);
	}

	return <div className='carousel'>
		<CarouselButton className='carousel__button--left' img={left} onClick={handleClickPrev}/>

		<div className='carousel__track-container'>
			<ul className='carousel__track' style={{transform: 'translateX(-' + (slideWidth * slide) + 'px)'}}>
				{props.elements.map((s, index) =>
					<SizeMe>{(p) => {
						setSlideWidth(p.size.width || 0);

						return <CarouselSlide img={s} key={index}
											  style={{left: (p.size.width || 0) * index}}/>;
					}}</SizeMe>
				)}
			</ul>
		</div>

		<CarouselButton className='carousel__button--right' img={right} onClick={handleClickNext}/>

		<div className='carousel__nav'>
			{props.elements.map((s, index) => <CarouselIndicator id={s}
																 className={slide === index ? 'current-slide' : ''}
																 onClick={() => handleIndicatorClick(index)}
																 key={index}/>)}
		</div>
	</div>;
}
